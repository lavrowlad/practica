library ieee;
use ieee.std_logic_1164.all;	 

-- Add your library and packages declaration here ...
USE IEEE.NUMERIC_STD.ALL;

entity tb_device is
	-- Generic declarations of the tested unit
		generic(
		D_WIDTH : POSITIVE := 8 );
end tb_device;

architecture arch_tb_device of tb_device is
	-- Component declaration of the tested unit
	component device
		generic(
		D_WIDTH : POSITIVE := 8 );
	port(
		clk : in STD_LOGIC;
		rst : in STD_LOGIC;
		write_en : in STD_LOGIC;
		read_en : in STD_LOGIC;
		data : in STD_LOGIC_VECTOR(D_WIDTH-1 downto 0);
		output : out STD_LOGIC_VECTOR(D_WIDTH downto 0) );
	end component;

	-- Stimulus signals - signals mapped to the input and inout ports of tested entity
	signal clk : STD_LOGIC;
	signal rst : STD_LOGIC;
	signal write_en : STD_LOGIC;
	signal read_en : STD_LOGIC;
	signal data : STD_LOGIC_VECTOR(D_WIDTH-1 downto 0);
	-- Observed signals - signals mapped to the output ports of tested entity
	signal output : STD_LOGIC_VECTOR(D_WIDTH downto 0);

	-- Add your code here ...
	-- Clock period definitions
	constant CLK_period : time := 10 ns;
begin

	-- Unit Under Test port map
	UUT : device
		generic map (
			D_WIDTH => D_WIDTH
		)

		port map (
			clk => clk,
			rst => rst,
			write_en => write_en,
			read_en => read_en,
			data => data,
			output => output
		);

	-- Add your stimulus here ...
	-- Clock process definitions
	clk_process :process
	begin
		clk <= '0';
		wait for CLK_period/2;
		clk <= '1';
		wait for CLK_period/2;
	end process;
	
	-- Reset process
	rst_proc : process
	begin
	wait for CLK_period * 5;		   
		rst <= '1';				  
		wait for CLK_period * 5;  
		rst <= '0';			   
		wait;
	end process;   
	
	-- Write process
	wr_proc : process
		variable counter : unsigned (7 downto 0) := (others => '0');
	begin		
		wait for CLK_period * 10;
		
		for i in 1 to 32 loop
			counter := counter + 1;
			data <= std_logic_vector(counter);	
			
			wait for CLK_period;
			write_en <= '1';
			wait for CLK_period; 
			write_en <= '1';
		end loop;
	end process;
	
	-- Read process
	rd_proc : process
	begin
		wait for CLK_period * 20;
			
		read_en <= '1';
		wait for CLK_period * 60;
		read_en <= '0';
		wait for CLK_period * 256 * 2;
		read_en <= '1'; 
		wait;
	end process;
end arch_tb_device;

configuration TESTBENCH_FOR_device of tb_device is
	for arch_tb_device
		for UUT : device
			use entity work.device(arch_device);
		end for;
	end for;
end TESTBENCH_FOR_device;

