SetActiveLib -work
comp -include "$DSN\src\component.vhd" 
comp -include "$DSN\src\TestBench\tb_comp.vhd" 
asim TESTBENCH_FOR_comp 
wave 
wave -noreg clk
wave -noreg rst
wave -noreg d_in
wave -noreg d_out
# The following lines can be used for timing simulation
# acom <backannotated_vhdl_file_name>
# comp -include "$DSN\src\TestBench\tb_comp_tim_cfg.vhd" 
# asim TIMING_FOR_comp 
