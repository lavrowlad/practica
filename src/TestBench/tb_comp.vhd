library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_signed.all;

	-- Add your library and packages declaration here ...

entity tb_comp is
	-- Generic declarations of the tested unit
		generic(
		D_IN_WIDTH : POSITIVE := 7;
		D_OUT_WIDTH : POSITIVE := 8 );
end tb_comp;

architecture arch_tb_comp of tb_comp is
	-- Component declaration of the tested unit
	component comp
		generic(
		D_IN_WIDTH : POSITIVE := 7;
		D_OUT_WIDTH : POSITIVE := 8 );
	port(
		clk : in STD_LOGIC;
		rst : in STD_LOGIC;
		d_in : in STD_LOGIC_VECTOR(D_IN_WIDTH downto 0);
		d_out : out STD_LOGIC_VECTOR(D_OUT_WIDTH downto 0) );
	end component;

	-- Stimulus signals - signals mapped to the input and inout ports of tested entity
	signal clk : STD_LOGIC;
	signal rst : STD_LOGIC;
	signal d_in : STD_LOGIC_VECTOR(D_IN_WIDTH downto 0);
	-- Observed signals - signals mapped to the output ports of tested entity
	signal d_out : STD_LOGIC_VECTOR(D_OUT_WIDTH downto 0);

	-- Add your code here ...
	signal CLK_period : time := 10 ns;
begin

	-- Unit Under Test port map
	UUT : comp
		generic map (
			D_IN_WIDTH => D_IN_WIDTH,
			D_OUT_WIDTH => D_OUT_WIDTH
		)

		port map (
			clk => clk,
			rst => rst,
			d_in => d_in,
			d_out => d_out
		);

	-- Add your stimulus here ...
	CLK_process :process
	begin
		clk <= '0';
		wait for CLK_period/2;
		clk <= '1';
		wait for CLK_period/2;
	end process;
	
	rst_proc : process
	begin
	wait for CLK_period * 5;
		rst <= '1';
		wait for CLK_period * 5;
		rst <= '0';		
		wait;
	end process;
	
	test_process :process
		variable counter : unsigned (7 downto 0) := (others => '0');
	begin
		wait for CLK_period * 20;

		for i in 1 to 32 loop
			d_in <= std_logic_vector(counter);		  
			wait for CLK_period * 1;
			counter := counter + 1;
		end loop;
	end process;
	
end arch_tb_comp;

configuration TESTBENCH_FOR_comp of tb_comp is
	for arch_tb_comp
		for UUT : comp
			use entity work.comp(arch_comp);
		end for;
	end for;
end TESTBENCH_FOR_comp;

