SetActiveLib -work
comp -include "$DSN\src\device.vhd" 
comp -include "$DSN\src\TestBench\tb_device.vhd" 
asim TESTBENCH_FOR_device 
wave 
wave -noreg clk
wave -noreg rst
wave -noreg write_en
wave -noreg read_en
wave -noreg data
wave -noreg output
# The following lines can be used for timing simulation
# acom <backannotated_vhdl_file_name>
# comp -include "$DSN\src\TestBench\tb_device_tim_cfg.vhd" 
# asim TIMING_FOR_device 
