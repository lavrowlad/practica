library ieee;
use ieee.std_logic_1164.all;   
use ieee.std_logic_arith.all;
use ieee.std_logic_signed.all;

entity device is
	Generic (
		constant D_WIDTH : positive := 8
	);
	Port (
		clk      : in  std_logic;
		rst      : in  std_logic;
		write_en : in  std_logic;
		read_en  : in  std_logic;
		data     : in  std_logic_vector(D_WIDTH - 1 downto 0);
		output   : out std_logic_vector(D_WIDTH     downto 0)
	);
end device;								   

architecture arch_device of device is
	--Components
	component comp
		Generic (
			constant D_IN_WIDTH  : positive := D_WIDTH - 1;
			constant D_OUT_WIDTH : positive := D_WIDTH
		);
		Port(
			clk   : in  std_logic;
			rst   : in  std_logic;
			d_in  : in  std_logic_vector(D_IN_WIDTH  downto 0);
			d_out : out std_logic_vector(D_OUT_WIDTH downto 0)
		);
	end component;
	component STD_FIFO
		Generic (
			constant DATA_WIDTH : positive := D_WIDTH;
			constant FIFO_DEPTH	: positive := D_WIDTH * 32
		);
		Port ( 
			CLK		: in  std_logic;
			RST		: in  std_logic;
			WriteEn	: in  std_logic;
			DataIn	: in  std_logic_vector (DATA_WIDTH - 1 downto 0);
			ReadEn	: in  std_logic;
			DataOut	: out std_logic_vector (DATA_WIDTH - 1 downto 0);
			Empty	: out std_logic;
			Full	: out std_logic
		);
	end component;
	
	-- Signals
	signal en_write : std_logic;
	signal en_read  : std_logic;
	signal is_empty : std_logic;
	signal is_full  : std_logic;
	
	signal d_fifo_input  : std_logic_vector(D_WIDTH-1 downto 0) := (others => 'Z');
	signal d_fifo_output : std_logic_vector(D_WIDTH-1 downto 0) := (others => 'Z');
	signal d_resoult     : std_logic_vector(D_WIDTH   downto 0) := (others => 'Z');
	
begin
	uut1 : STD_FIFO port map(
			CLK		=> clk,
			RST		=> rst,
			WriteEn	=> en_write,
			DataIn	=> d_fifo_input,
			ReadEn	=> en_read,
			DataOut	=> d_fifo_output,
			Empty	=> is_empty,
			Full	=> is_full
		);
	uut2 : comp port map(
			clk   => clk,
			rst   => rst,
			d_in  => d_fifo_output,
			d_out => d_resoult
		);

	clk_process :process (clk)
		variable counter : unsigned (7 downto 0) := (others => '0');
	begin
		if rising_edge(CLK) then
			if RST = '1' then
				d_resoult <= (others => 'Z');
				en_write  <= '0';
				en_read   <= '0';
				is_empty  <= '1';
				is_full   <= '0';
			else				 
				d_fifo_input <= data;
				en_read  <= read_en;
				en_write <= write_en;
			end if;
			output <= d_resoult;
		end if;
	end process;
end arch_device;











