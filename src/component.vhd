library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_signed.all;

entity comp is
	Generic (
		constant D_IN_WIDTH  : positive := 8 - 1;
		constant D_OUT_WIDTH : positive := 8
	);
	Port(
		clk   : in  std_logic;
		rst   : in  std_logic;
		d_in  : in  std_logic_vector(D_IN_WIDTH  downto 0);
		d_out : out std_logic_vector(D_OUT_WIDTH downto 0)
	);
end comp;

architecture arch_comp of comp is
begin
	clk_process : process (clk)
		variable data : std_logic_vector(D_OUT_WIDTH downto 0) := (others => 'Z');
	begin	
		if rising_edge(CLK) then	   	
			if RST = '1' then			   
				data  := (others => 'Z');
				d_out <= (others => 'Z');
			else
				data := '0' & d_in;
				data := data + data;
				d_out <= data;	
			end if;
		end if;
	end process;
end arch_comp;